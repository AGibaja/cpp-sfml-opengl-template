#include <SFML/Graphics.hpp>
#include <glew.h>

#include <GL/gl.h>
#include <SFML/OpenGL.hpp>

int main()
{
  glewInit();
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);
    
    
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::KeyPressed)
              window.close();
        }

        window.clear();
        window.draw(shape);
        window.display();
    }

    return 0;
}

